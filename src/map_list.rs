use std::collections::btree_map::Entry;
use std::collections::BTreeMap;

pub struct MapList<K, V> {
    inner: BTreeMap<K, Vec<V>>
}

impl <K, V> MapList<K, V> 
    where K: 
        std::cmp::Ord + Clone
{
    pub fn new() -> Self {
        MapList {
            inner: BTreeMap::new()
        }
    }
    
    pub fn set(&mut self, key: K, mut values: Vec<V>) {
        let v = self.inner.entry(key).or_insert(Vec::new());
        v.splice(.., values.drain(..)).collect::<Vec<V>>();
    }

    pub fn insert(&mut self, key: K, value: V) {
        self.inner.entry(key).or_insert(Vec::new()).push(value);
    }

    pub fn contains_key(&self, key: K) -> bool {
        self.inner.contains_key(&key)
    }

    pub fn clear(&mut self) {
        self.inner.clear();
    }

    pub fn get(&mut self, key: K) -> Option<&Vec<V>> {
        self.inner.get(&key)
    }
    
    pub fn pop_front(&mut self) -> Option<V> {
        let key = self.get_low_key();
        if let Entry::Occupied(mut entry) = self.inner.entry(key) {
            let list = entry.get_mut();
            let value = list.remove(0);
            if list.is_empty() {
                entry.remove_entry();
            }
            
            return Some(value);
        }
        
        None
    }
    
    pub fn is_empty(&self) -> bool {
        self.inner.is_empty()
    }
    
    fn get_low_key(&self) -> K {
        let min = self.inner.iter().next();
        let (k, _) = min.unwrap();
        k.clone()
    }
}