use screeps::objects::Creep;
use stdweb::private::ConversionError;
use log::*;
use screeps::{game, find, prelude::*, objects::Room, pathfinder::*, LookResult, constants::*};

pub struct CreepController {
}

impl CreepController {
    pub fn new() -> Self {
        CreepController {
        }
    }

    pub fn run(&self) -> Result<(), ConversionError> {
        debug!("running creeps");
        for creep in screeps::game::creeps::values() {
            if creep.spawning() { continue; }

            let role: Option<String> = creep.memory().string("role")?;
            if let Some(r) = role {
                match r.as_ref() {
                    "miner" => self.do_miner(&creep),
                    "truck" => self.do_truck(&creep),
                    "worker" => self.do_worker(&creep),
                    _ => {}
                }
            }
        }

        Ok(())
    }

    fn do_miner(&self, creep: &Creep) {
        let _ = creep.memory()
            .string("source")
            .and_then(|id| game::get_object_typed::<screeps::objects::Source>(&id.unwrap()))
            .and_then(|s| {
                let source = s.unwrap();
                if creep.harvest(&source) == ReturnCode::NotInRange {
                    creep.move_to(&source);
                }

                Ok(())
            });
    }

    fn do_truck(&self, creep: &Creep) {
        let _ = creep.memory()
            .string("task")
            .and_then(|task| {
                if let None = task {
                    creep.memory().set("task", "load");
                    return Ok(());
                }

                match task.unwrap().as_ref() {
                    "load" => {
                        if creep.carry_total() >= creep.carry_capacity() {
                            creep.memory().set("task", "deliver");
                            return Ok(());
                        }

                        let _ = creep.memory()
                            .string("source")
                            .and_then(|id| game::get_object_typed::<screeps::objects::Source>(&id.unwrap()))
                            .and_then(|s| {
                                let source = s.unwrap();
                                if creep.pos().get_range_to(&source) < 3 {
                                    let resources = creep.pos().find_in_range(find::DROPPED_RESOURCES, 2);
                                    if !resources.is_empty() {
                                        let drop = &resources[0];
                                        if creep.pickup(&drop) == ReturnCode::NotInRange {
                                            creep.move_to(drop);
                                        }
                                    }
                                } else {
                                    creep.move_to(&source);
                                }

                                Ok(())
                            });                            
                    },

                    "deliver" => {
                        if creep.carry_total() == 0 {
                            creep.memory().set("task", "load");
                            return Ok(());
                        }

                        let _ = creep.room()
                            .find(find::MY_SPAWNS)
                            .iter()
                            .nth(0)
                            .and_then(|s| {
                                if creep.transfer_all(s, ResourceType::Energy) == ReturnCode::NotInRange {
                                    creep.move_to(s);
                                }

                                None::<ReturnCode>
                            });
                    },

                    _ => {}
                };

                Ok(())
            });
    }

    fn do_worker(&self, creep: &Creep) {
        let _ = creep.memory()
            .string("task")
            .and_then(|task| {
                if let None = task {
                    creep.memory().set("task", "load");
                    return Ok(());
                }

                match task.unwrap().as_ref() {
                    "load" => {
                        if creep.carry_total() >= creep.carry_capacity() {
                            creep.memory().set("task", "build");
                            return Ok(());
                        }

                        let resource = creep.pos().find_closest_by_range(find::DROPPED_RESOURCES);
                        if let None = resource {
                            return Ok(());
                        }

                        let resource = resource.unwrap();
                        if creep.pickup(&resource) == ReturnCode::NotInRange {
                            creep.move_to(&resource);
                        }
                    },

                    "build" => {
                        if creep.carry_total() == 0 {
                            creep.memory().set("task", "load");
                            return Ok(());
                        }

                        let site = creep.pos().find_closest_by_range(find::CONSTRUCTION_SITES);
                        if let None = site {
                            creep.memory().set("task", "upgrade");
                            return Ok(());
                        }

                        let site = site.unwrap();
                        if creep.build(&site) == ReturnCode::NotInRange {
                            creep.move_to(&site);
                        }
                    },

                    "upgrade" => {
                        if creep.carry_total() == 0 {
                            creep.memory().set("task", "load");
                            return Ok(());
                        }

                        if let None = creep.room().controller() {
                            return Ok(());
                        }

                        let controller = creep.room().controller().unwrap();
                        if creep.upgrade_controller(&controller) == ReturnCode::NotInRange {
                            creep.move_to(&controller);
                        }
                    },

                    _ => { }
                }

                Ok(())
            });
    }
}