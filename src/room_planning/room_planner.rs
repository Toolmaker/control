use crate::togglemap_2d::ToggleMap2D;

use screeps::StructureType;
use screeps::objects::Room;
use screeps::RoomPosition;
use screeps::find;
use screeps::look;
use screeps::objects::HasPosition;
use screeps::objects::StructureProperties;
use log::*;

use crate::map::Map;
use crate::movement_cost_map::*;
use crate::map_list::*;
use crate::room_planning::templates::*;

const TILE_NEW: u8 = 0;
const TILE_OPEN: u8 = 1;
const TILE_CLOSED: u8 = 2;
const ROW_LENGTH: u8 = 50;

pub struct RoomPlanner {
    room: Room,
    map: Map,
    start_pos: RoomPosition,
    movement_cost: MovementCostMap,
    open_list: MapList<u32, RoomPosition>,
    tiles: ToggleMap2D,
    planned_structures: Vec<(u8, u8, StructureType)>
}

impl RoomPlanner {
    pub fn new(room: Room) -> Self {
        let mut map = Map::new();
        map.index(room.clone());
        let start_pos = RoomPlanner::select_start_pos(&room);
        
        let mut result = RoomPlanner {
            room: room.clone(),
            movement_cost: MovementCostMap::new(&room, &map, start_pos),
            map: map,
            start_pos: start_pos,
            open_list: MapList::new(),
            tiles: ToggleMap2D::new(2500, 2, 50),
            planned_structures: vec!()
        };

        result.reset_openlist();
        result
    }

    pub fn plan(&mut self) -> Vec<(u8, u8, StructureType)> {
        if !self.place_spawn() {
            return Vec::new();
        }

        self.reset_openlist();
        self.place_site(&HUGE_EXTENSION_MATRIX);
        self.reset_openlist();
        self.place_site(&CORE_MATRIX);
        self.reset_openlist();
        self.place_site(&LAB_MATRIX);

        self.planned_structures.to_vec()
    }

    fn reset_openlist(&mut self) {
        self.open_list.clear();
        self.tiles.reset();

        self.open_list.insert(0, self.start_pos);
        self.add_adjacent_tiles(self.start_pos);
        self.tiles.set(self.start_pos.x() as usize, self.start_pos.y() as usize, TILE_OPEN);
    }

    fn place_spawn(&mut self) -> bool {
        let pos = self.translate_position(self.start_pos, RoomPlanner::get_template_size(&SPAWN_MATRIX));
        if !pos.is_some() {
            return false;
        }

        let pos = pos.unwrap();
        if self.can_build(pos, &SPAWN_MATRIX) {
            self.build(pos, &SPAWN_MATRIX);
            return true;
        }

        info!("Error placing spawn matrix, aborting room planning.");
        false
    }

    fn place_site(&mut self, template: &[&[char]]) {
        let mut found = false;

        while !found && !self.open_list.is_empty() {
            let mut value = self.open_list.pop_front().unwrap();
            self.set_closed(value);
            self.add_adjacent_tiles(value);

            let pos = self.translate_position(value, RoomPlanner::get_template_size(&template));
            if !pos.is_some() { continue; }
            
            value = pos.unwrap();
            found = self.can_build(value, template);

            if found {
                self.build(value, template);
            }
        }
    }

    fn get_template_size(template: &[&[char]]) -> (usize, usize) {
        (template[0].len(), template.len())
    }

    fn select_start_pos(room: &Room) -> RoomPosition {
        let spawn = &room.find(find::MY_SPAWNS)[0];
        spawn.pos()
    }

    fn add_adjacent_tiles(&mut self, pos: RoomPosition) {
        let start_x: i32 = (pos.x() as i32) - 1;
        let start_y: i32 = (pos.y() as i32) - 1;

        for y in start_y..pos.y() as i32 + 2 {
            for x in start_x..pos.x() as i32 + 2 {
                if x < 0 || y < 0 { continue; }
                if y >= ROW_LENGTH as i32 || x >= ROW_LENGTH as i32 { continue; }
                if x == pos.x() as i32 && y == pos.y() as i32 { continue; }

                let p = self.room.get_position_at(x as u32, y as u32).unwrap();
                if self.get_tile_state(p) == TILE_NEW {
                    self.push_tile(p);
                }
            }
        }
    }

    fn push_tile(&mut self, pos: RoomPosition) {
        let weight = self.movement_cost.get_weight(pos) as u32;
        self.set_open(pos);
        self.open_list.insert(weight, pos);
    }

    fn set_open(&mut self, pos: RoomPosition) {
        self.tiles.set(pos.x() as usize, pos.y() as usize, TILE_OPEN);
    }

    fn set_closed(&mut self, pos: RoomPosition) {
        self.tiles.set(pos.x() as usize, pos.y() as usize, TILE_CLOSED);
    }

    fn get_tile_state(&self, pos: RoomPosition) -> u8 {
        self.tiles.get(pos.x() as usize, pos.y() as usize)
    }

    fn can_build(&self, start_pos: RoomPosition, template: &[&[char]]) -> bool {
        let pos = start_pos;
        for r in 0..template.len() {
            for c in 0..template[r].len() {
                let x = pos.x() as usize + c;
                let y = pos.y() as usize + r;

                let pos = self.room.get_position_at(x as u32, y as u32);
                if !pos.is_some() { continue; } 

                let mask = template[r][c];
                match mask {
                    'Y' => continue,
                    'R' => if !self.map.is_buildable(x,  y) { return false },
                    'N' => if !self.map.is_buildable(x, y) { return false },
                    t => {
                        let structure_type = RoomPlanner::mask_to_structure_type(t);
                        if self.map.is_road(x, y) { return false; }

                        if !self.map.is_buildable(x, y) && structure_type != self.structure_type_at(pos.unwrap()) {
                            return false;
                        }
                    }
                }
            }
        }

        true
    }

    fn translate_position(&self, pos: RoomPosition, template_size: (usize, usize)) -> Option<RoomPosition> {
        let dx = ((template_size.0 as f32) / 2.0).floor() as i32;
        let dy = ((template_size.1 as f32) / 2.0).floor() as i32;
        let x = pos.x() as i32 - dx;
        let y = pos.y() as i32 - dy;

        if x < 0 || y < 0 { return None; }
        self.room.get_position_at(x as u32, y as u32)
    }

    fn build(&mut self, pos: RoomPosition, template: &[&[char]]) {
        for r in 0..template.len() {
            for c in 0..template[r].len() {
                let x = pos.x() as usize + c;
                let y = pos.y() as usize + r;
                let mask = template[r][c];

                if mask == 'Y' || mask == 'N' || mask == 'R' { continue; }
                let struc_type = RoomPlanner::mask_to_structure_type(mask);
                self.map.set_structure(x, y);
                self.planned_structures.push((x as u8, y as u8, struc_type.unwrap()));
            }
        }
    }

    fn mask_to_structure_type(mask: char) -> Option<StructureType> {
        match mask {
            'E' => return Some(StructureType::Extension),
            'T' => return Some(StructureType::Tower),
            'C' => return Some(StructureType::Container),
            'X' => return Some(StructureType::Extractor),
            'L' => return Some(StructureType::Lab),
            'I' => return Some(StructureType::Link),
            'U' => return Some(StructureType::Nuker),
            'O' => return Some(StructureType::Observer),
            'P' => return Some(StructureType::PowerSpawn),
            'A' => return Some(StructureType::Rampart),
            'R' => return Some(StructureType::Road),
            'S' => return Some(StructureType::Spawn),
            'M' => return Some(StructureType::Terminal),
            'G' => return Some(StructureType::Storage),
            _ => return None
        }
    }

    fn structure_type_at(&self, pos: RoomPosition) -> Option<StructureType> {
        let planned = self.planned_structures.iter().find(|t| t.0 as u32 == pos.x() && t.1 as u32 == pos.y());
        if let Some(t) = planned {
            return Some(t.2);
        }

        let construction_sites = pos.look_for(look::CONSTRUCTION_SITES);
        if !construction_sites.is_empty() {
            return Some(construction_sites[0].structure_type());
        }

        let structures = pos.look_for(look::STRUCTURES);
        if !structures.is_empty() {
            return Some(structures[0].structure_type());
        }

        None
    }

    /* Potential structures:
Container   => C
Extension   => E
Extractor   => X
Lab         => L
Link        => I
Nuker       => N
Observer    => O
PowerSpawn  => P
Rampart     => A
Road        => R
Spawn       => S
Storage     => G
Terminal    => M
Tower       => T
Wall        => W
Nothing     => N
Anything    => Y
*/
}