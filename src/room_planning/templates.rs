/* Potential structures:
    Container   => C
    Extension   => E
    Extractor   => X
    Lab         => L
    Link        => I
    Nuker       => U
    Observer    => O
    PowerBank   => B
    PowerSpawn  => P
    Rampart     => A
    Road        => R
    Spawn       => S
    Storage     => G
    Terminal    => M
    Tower       => T
    Wall        => W
    Nothing     => N
    Anything    => Y
*/

#[allow(dead_code)]
pub const SPAWN_MATRIX: [&[char]; 2] = [
    &['S', 'N', 'S'],
    &['N', 'S', 'N']
];

#[allow(dead_code)]
pub const LAB_MATRIX: [&[char]; 6] = [
    &['N', 'N', 'N', 'N', 'N', 'N'],
    &['N', 'N', 'L', 'L', 'N', 'N'],
    &['N', 'L', 'N', 'L', 'L', 'N'],
    &['N', 'L', 'L', 'N', 'L', 'N'],
    &['N', 'N', 'L', 'L', 'N', 'N'],
    &['N', 'N', 'N', 'N', 'N', 'N']
];

#[allow(dead_code)]
pub const CORE_MATRIX: [&[char]; 5] = [
    &['N', 'N', 'N', 'N', 'N'],
    &['N', 'P', 'U', 'M', 'N'],
    &['N', 'O', 'N', 'G', 'N'],
    &['N', 'I', 'N', 'T', 'N'],
    &['N', 'N', 'N', 'N', 'N']
];

#[allow(dead_code)]
pub const HUGE_EXTENSION_MATRIX: [&[char]; 13] = [
    &['Y', 'Y', 'Y', 'Y', 'N', 'N', 'N', 'N', 'N', 'Y', 'Y', 'Y', 'Y'],
    &['Y', 'Y', 'Y', 'N', 'N', 'E', 'E', 'E', 'N', 'N', 'Y', 'Y', 'Y'],
    &['Y', 'Y', 'N', 'N', 'E', 'E', 'N', 'E', 'E', 'N', 'N', 'Y', 'Y'],
    &['Y', 'N', 'N', 'E', 'E', 'N', 'E', 'N', 'E', 'E', 'N', 'N', 'Y'],
    &['N', 'N', 'E', 'E', 'N', 'E', 'E', 'E', 'N', 'E', 'E', 'N', 'N'],
    &['N', 'E', 'E', 'N', 'E', 'E', 'E', 'C', 'E', 'N', 'E', 'E', 'N'],
    &['N', 'N', 'N', 'E', 'E', 'E', 'L', 'E', 'E', 'E', 'N', 'N', 'N'],
    &['N', 'E', 'E', 'N', 'E', 'C', 'E', 'E', 'E', 'N', 'E', 'E', 'N'],
    &['N', 'N', 'E', 'E', 'N', 'E', 'E', 'E', 'N', 'E', 'E', 'N', 'N'],
    &['Y', 'N', 'N', 'E', 'E', 'N', 'E', 'N', 'E', 'E', 'N', 'N', 'Y'],
    &['Y', 'Y', 'N', 'N', 'E', 'E', 'N', 'E', 'E', 'N', 'N', 'Y', 'Y'],
    &['Y', 'Y', 'Y', 'N', 'N', 'E', 'E', 'E', 'N', 'N', 'Y', 'Y', 'Y'],
    &['Y', 'Y', 'Y', 'Y', 'N', 'N', 'N', 'N', 'N', 'Y', 'Y', 'Y', 'Y']
];

#[allow(dead_code)]
pub const SMALL_EXTENSION_MATRIX: [&[char]; 5] = [
    &['Y', 'Y', 'N', 'Y', 'Y'],
    &['Y', 'N', 'E', 'N', 'Y'],
    &['N', 'E', 'E', 'E', 'N'],
    &['Y', 'N', 'E', 'N', 'Y'],
    &['Y', 'Y', 'N', 'Y', 'Y']
];

#[allow(dead_code)]
pub const MEDIUM_EXTENSION_MATRIX: [&[char]; 6] = [
    &['Y', 'Y', 'N', 'N', 'Y', 'Y'],
    &['Y', 'N', 'E', 'E', 'N', 'Y'],
    &['N', 'E', 'E', 'E', 'E', 'N'],
    &['N', 'E', 'E', 'E', 'E', 'N'],
    &['Y', 'N', 'E', 'E', 'N', 'Y'],
    &['Y', 'Y', 'N', 'N', 'Y', 'Y']
];