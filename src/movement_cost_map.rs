use crate::map::Map;
use screeps::objects::Room;
use screeps::RoomPosition;
use crate::togglemap_2d::*;

pub struct MovementCostMap {
    size: usize,
    row_length: usize,
    tiles: Vec<u16>,
    open: Vec<RoomPosition>
}

impl MovementCostMap {
    pub fn new(room: &Room, map: &Map, start: RoomPosition) -> Self {
        let terrain = map.terrain();
        let mut map = MovementCostMap {
            size: terrain.length() * terrain.items_per_byte(),
            row_length: terrain.row_length(),
            tiles: vec!(0; terrain.length() * terrain.items_per_byte()),
            open: Vec::new()
        };

        map.fill(room, terrain, start);
        map
    }

    #[allow(dead_code)]
    pub fn size(&self) -> usize {
        self.size
    }

    fn fill(&mut self, room: &Room, terrain: &ToggleMap2D, start: RoomPosition) {
        self.open.push(start.clone());
        self.set_weight(terrain, start.clone(), 0);

        while !self.open.is_empty() {
            let pos = self.open[0].clone();
            self.add_adjacent(room, terrain, pos);
            self.open.remove(0);
        }
    }

    fn set_weight(&mut self, terrain: &ToggleMap2D, pos: RoomPosition, curr_weight: u16) {
        let idx = (pos.y() * self.row_length as u32 + pos.x()) as usize;
        let terrain_type = terrain.get(pos.x() as usize, pos.y() as usize);
        match terrain_type {
            0 => self.tiles[idx] = curr_weight + 1,
            2 => self.tiles[idx] = curr_weight + 5,
            _ => self.tiles[idx] = std::u16::MAX
        }
    }

    pub fn get_weight(&self, pos: RoomPosition) -> u16 {
        let idx = (pos.y() * self.row_length as u32 + pos.x()) as usize;
        self.tiles[idx]
    }

    fn add_adjacent(&mut self, room: &Room, terrain: &ToggleMap2D, pos: RoomPosition) {
        let weight = self.get_weight(pos.clone());
        let max_row_index = (self.row_length - 1) as i32;
        let mut start_y = (pos.y() - 1) as i32 ;
        let mut end_y = (pos.y() + 1) as i32 ;
        let mut start_x = (pos.x() - 1) as i32;
        let mut end_x = (pos.x() + 1) as i32;

        if start_y < 0 { start_y = 0; }  
        if start_x < 0 { start_x = 0; }
        if end_y > max_row_index { end_y = max_row_index; }
        if end_x > max_row_index { end_x = max_row_index; }

        for y in start_y..end_y + 1 {
            for x in start_x..end_x + 1 {
                if y > max_row_index { continue; }
                if x > max_row_index { continue; }
                if x as u32 == pos.x() && y as u32 == pos.y() { continue; }

                let p = room.get_position_at(x as u32, y as u32).unwrap();
                let tile_weight = self.get_weight(p.clone());
                if tile_weight == 0 {
                    self.set_weight(terrain, p.clone(), weight);
                    self.open.push(p);
                }
            }
        }
    }
}