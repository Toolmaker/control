#![recursion_limit = "128"]
mod logging;

use screeps::PositionedLookResult;
use screeps::RoomPosition;
use std::collections::HashSet;

use log::*;
use screeps::{find, prelude::*, objects::Room, pathfinder::*, LookResult, constants::*};
use stdweb::{js};
use serde_json;

mod math;
mod map;
mod map_list;
mod togglemap_2d;
mod movement_cost_map;
mod room_planning;
mod creep_controller;
mod spawn_controller;

fn main() {  
    stdweb::initialize();
    logging::setup_logging(logging::Info);

    js! {
        var game_loop = @{game_loop};

        module.exports.loop = function() {
            // Provide actual error traces.
            try {
                game_loop();
            } catch (error) {
                // console_error function provided by 'screeps-game-api'
                console_error("caught exception:", error);
                if (error.stack) {
                    console_error("stack trace:", error.stack);
                }
                console_error("resetting VM next tick.");
                // reset the VM since we don't know if everything was cleaned up and don't
                // want an inconsistent state.
                module.exports.loop = wasm_initialize;
            }
        }
    }
}

fn game_loop() {
    let time = screeps::game::time();

    debug!("loop starting! CPU: {}", screeps::game::cpu::get_used());
    debug!("running spawns");

    let controller = creep_controller::CreepController::new();
    match controller.run() {
        Result::Ok(_r) => info!("Success run"),
        Result::Err(_e) => info!("Error")
    }

    for room in screeps::game::rooms::values() {
        let mut spawn = spawn_controller::SpawnController::new(&room);
        spawn.run();
        // if time % 10 == 0 {
        //     info!("Cleaning up construction sites");
        //     let sites = room.find(find::CONSTRUCTION_SITES);
        //     for site in sites {
        //         site.remove();
        //     }
        // } else {
        //     plan_containers(room.clone());

        //     if time % 10 == 1 {
        //         plan_room(room.clone());
        //     }
        // }   
    }
    
    if time % 32 == 3 {
        info!("running memory cleanup");
        cleanup_memory().expect("expected Memory.creeps format to be a regular memory object");
    }

    //info!("done! cpu: {}", screeps::game::cpu::get_used())
}

fn cleanup_memory() -> Result<(), Box<dyn ::std::error::Error>> {
    let alive_creeps: HashSet<String> = screeps::game::creeps::keys().into_iter().collect();

    let screeps_memory = match screeps::memory::root().dict("creeps")? {
        Some(v) => v,
        None => {
            warn!("not cleaning game creep memory: no Memory.creeps dict");
            return Ok(());
        }
    };

    for mem_name in screeps_memory.keys() {
        if !alive_creeps.contains(&mem_name) {
            debug!("cleaning up creep memory of dead creep {}", mem_name);
            screeps_memory.del(&mem_name);
        }
    }

    Ok(())
}

fn count_in_range<T, F>(pos: RoomPosition, find_type: T, range: u32, filter: F) -> u32     
where 
    T : FindConstant,
    for<'r> F : FnMut(&'r T::Item) -> bool
{
    pos
		.find_in_range(find_type, range)
		.into_iter()
		.filter(filter)
        .count() as u32
}

fn plan_containers(room: Room) {
    let spawns = room.find(find::MY_SPAWNS);
    if spawns.len() == 0 {
        return;
    }

    let spawn = &spawns[0];
    let sources = room.find(find::SOURCES);
    //info!("Found {} sources", sources.len());
    for source in &sources {
        let pos: RoomPosition = source.pos();

        let container_count = count_in_range(source.pos(), find::STRUCTURES, 1, |s| s.structure_type() == StructureType::Container);
        let construction_count = count_in_range(source.pos(), find::CONSTRUCTION_SITES, 1, |s| s.structure_type() == StructureType::Container);
        if construction_count + container_count > 0 { continue; }

        let left = pos.x() - 1;
        let right = pos.x() + 1;
        let top = pos.y() - 1;
        let bottom = pos.y() + 1;

        let look_result = room.look_at_area(top, left, bottom, right);
        let terrain: Vec<PositionedLookResult> = look_result
            .into_iter()
            .filter(|t| t.x != pos.x() || t.y != pos.y())
            .filter(|t| 
                match t.look_result {
                    LookResult::Terrain(Terrain::Wall)  => false,
                    LookResult::Terrain(_) => true,
                    _ => false
                }
            )
            .collect();

        let tuple = terrain
            .iter()
            .map(|t| {
                let adjacent_tiles = terrain.iter().fold(0, |acc, o| match dist(t.x, t.y, o.x, o.y) { 1 => acc + 1, _ => acc});
                let path = search(&room.get_position_at(t.x, t.y).unwrap(), &spawn.pos(), 0, SearchOptions::default());
                (t, path.cost - adjacent_tiles, adjacent_tiles)
            })
            .min_by(|a, b| (a.1 - a.2).partial_cmp(&(b.1 - b.2)).unwrap())
            .unwrap();

        let tile = tuple.0;
        let pos = room.get_position_at(tile.x, tile.y).unwrap();
        let result = room.create_construction_site(&pos, StructureType::Container);
        match result {
            ReturnCode::Ok => /*info!("Successfully created site at ({}, {})", tile.x, tile.y)*/ { },
            _ => info!("Error creating site at ({}, {}): {}", tile.x, tile.y, result as u16)
        }
    }
}

fn dist(x1: u32, y1: u32, x2: u32, y2: u32) -> u32 {
    ((x1 - x2).pow(2) as f64 + (y1 - y2).pow(2) as f64).sqrt() as u32
}

fn plan_room(room: Room) {
    let mut planner = room_planning::RoomPlanner::new(room.clone());
    let layout = planner.plan();
}



// fn run_spawns() {
//     for spawn in screeps::game::spawns::values() {
//         debug!("running spawn {}", spawn.name());
//         let body = [Part::Move, Part::Move, Part::Carry, Part::Work];

//         if spawn.energy() >= body.iter().map(|p| p.cost()).sum() {
//             // create a unique name, spawn.
//             let name_base = screeps::game::time();
//             let mut additional = 0;
//             let res = loop {
//                 let name = format!("{}-{}", name_base, additional);
//                 let mut opts = SpawnOptions::new();
//                 let mem = MemoryReference::new();
//                 mem.set("role", "miner");
//                 opts = opts.memory(mem);
//                 let res = spawn.spawn_creep_with_options(&body, &name, &opts);

//                 if res == ReturnCode::NameExists {
//                     additional += 1;
//                 } else {
//                     break res;
//                 }
//             };

//             if res != ReturnCode::Ok {
//                 warn!("couldn't spawn: {:?}", res);
//             }
//         }
//     }
// }
