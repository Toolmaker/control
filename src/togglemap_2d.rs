const BITS_PER_BYTE: usize = 8;

pub struct ToggleMap2D {
    items_per_byte: usize,
    row_length: usize,
    data: Vec<u8>,
    mask: u8
}

impl ToggleMap2D {
    pub fn new(num_items: usize, bits: usize, row_length: usize) -> Self {
        let items_per_byte = BITS_PER_BYTE / bits;
        let length = (num_items as f32 / items_per_byte as f32).ceil() as usize;
        let mut mask = 1;
        for n in 0..bits {
            mask = mask | (1 << n);
        }
        
        ToggleMap2D {
            items_per_byte: items_per_byte,
            row_length: row_length,
            data: vec!(0; length),
            mask: mask
        }
    }

    fn cutoff_value(&self, value: u8) -> u8 {
        value & self.mask
    }
    
    pub fn items_per_byte(&self) -> usize {
        self.items_per_byte
    }
    pub fn length(&self) -> usize {
        self.data.len()
    }

    pub fn row_length(&self) -> usize {
        self.row_length
    }
    
    pub fn get_byte_bit(&self, x: usize, y: usize) -> (usize, usize) {
        let byte = (y * self.row_length + x) / self.items_per_byte;
        let bit = ((y * self.row_length + x) % self.items_per_byte) * (8 / self.items_per_byte);

        (byte, bit)
    }

    pub fn set(&mut self, x: usize, y: usize, value: u8) {
        let (byte, bit) = self.get_byte_bit(x, y);
        let mask = self.cutoff_value(value) << bit;
        self.data[byte] = (self.data[byte] & (!mask)) | mask;
    }

    pub fn get(&self, x: usize, y: usize) -> u8 {
        let (byte, bit) = self.get_byte_bit(x, y);
        (self.data[byte] & (self.mask << bit)) >> bit
    }

    pub fn reset(&mut self) {
        self.data = vec!(0; self.data.len());
    }
}