use screeps::*;

use crate::togglemap_2d::*;

const MAX_TILES: usize = 50 * 50;
const MAX_MAP_ARRAY_LENTH: usize = MAX_TILES/8+1;
const ROW_LENGTH: usize = 50;

pub struct Map {
    terrain: ToggleMap2D,
    structures: [u8; MAX_MAP_ARRAY_LENTH],
    roads: [u8; MAX_MAP_ARRAY_LENTH]
}

impl Map {
    pub fn new() -> Self {
        Map {
            terrain: ToggleMap2D::new(MAX_TILES, 2, ROW_LENGTH),
            structures: [0; MAX_MAP_ARRAY_LENTH],
            roads: [0; MAX_MAP_ARRAY_LENTH]
        }
    }

    pub fn index(&mut self, room: Room) {
        self.index_terrain(room.clone());
        //self.index_roads(room);
        self.index_structures(room.clone());
    }

    fn index_terrain(&mut self, room: Room) {
        let terrain = room.get_terrain();
        let raw = terrain.get_raw_buffer();

        for r in 0..ROW_LENGTH - 1 {
            for c in 0..ROW_LENGTH - 1 {
                let terrain_value = raw[r * (ROW_LENGTH) + c];

                if terrain_value & TERRAIN_MASK_SWAMP == TERRAIN_MASK_SWAMP {
                    self.terrain.set(c, r, TERRAIN_MASK_SWAMP);
                } else if terrain_value & TERRAIN_MASK_WALL == TERRAIN_MASK_WALL {
                    self.terrain.set(c, r, TERRAIN_MASK_WALL);
                } else {
                    self.terrain.set(c, r, 0);
                }
            }
        }
    }

    fn index_structures(&mut self, room: Room) {
        let structures = room.find(find::STRUCTURES);
        for s in structures {
            self.set_structure(s.pos().x() as usize, s.pos().y() as usize);
        }

        let construction_sites = room.find(find::CONSTRUCTION_SITES);
        for s in construction_sites {
            self.set_structure(s.pos().x() as usize, s.pos().y() as usize);
        }
    }

    fn get_byte_bit(&self, x: usize, y: usize) -> (usize, usize) {
        let byte = (y * ROW_LENGTH + x) / 8;
        let bit = (y * ROW_LENGTH + x) % 8;

        (byte, bit)
    }

    pub fn set_structure(&mut self, x: usize, y: usize) {
        let (byte, bit) = self.get_byte_bit(x, y);
        self.structures[byte] |= 1 << bit;
    }

    #[allow(dead_code)]
    fn set_road(&mut self, x: usize, y: usize) {
        let (byte, bit) = self.get_byte_bit(x, y);
        self.roads[byte] |= 1 << bit;
    }

    pub fn has_structure(&self, x: usize, y: usize) -> bool {
        let (byte, bit) = self.get_byte_bit(x, y);
        let mask = 1 << bit;

        self.structures[byte] & mask == mask
    }

    pub fn is_buildable(&self, x: usize, y: usize) -> bool {
        self.is_traversable(x, y) && !self.has_structure(x, y)
     }

    pub fn is_traversable(&self, x: usize, y: usize) -> bool {
        let terrain = self.terrain.get(x, y);
        terrain == 0 || terrain == TERRAIN_MASK_SWAMP
    }

    #[allow(dead_code, unused_variables)]
    pub fn is_road(&self, x: usize, y: usize) -> bool {
        false
    }

    pub fn terrain(&self) -> &ToggleMap2D {
        &self.terrain
    }
}