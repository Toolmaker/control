use screeps::{ HasPosition };

pub struct Math {
}

impl Math {
    pub fn dist(left: &HasPosition, right: &HasPosition) -> u32 {
        let x1 = left.pos().x();
        let y1 = left.pos().y();
        let x2 = right.pos().x();
        let y2 = right.pos().y();

        ((x1 - x2).pow(2) as f64 + (y1 - y2).pow(2) as f64).sqrt().abs() as u32
    }
}