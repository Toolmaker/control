use crate::map_list::MapList;
use itertools::Itertools;
use std::cmp::Ordering;
use log::*;
use screeps::{ constants::Part, constants::HARVEST_POWER, find, objects::Creep, objects::Room, objects::Source, objects::StructureSpawn };
use crate::math::Math;

pub struct SpawnController<'a> {
    room: &'a Room,
    creeps: MapList<String, Creep>,
    spawns: Vec<StructureSpawn>
}

impl<'a> SpawnController<'a> {
    pub fn new(room: &'a Room) -> Self {
        SpawnController {
            room: room,
            creeps: MapList::new(),
            spawns: Vec::new()
        }
    }

    pub fn run(&mut self) {
        self.init();
        
        if !self.can_spawn() { return; }

        self.spawn_miners_and_trucks();
    }

    fn init(&mut self) {
        self.collect_creeps();

        self.spawns = self.room.find(find::MY_SPAWNS)
            .drain(..)
            .filter(|s| !s.is_spawning())
            .collect();
    }

    fn collect_creeps(&mut self) {
        let grouping = self.room
            .find(find::MY_CREEPS)
            .into_iter()
            .group_by(|c| 
                c.memory()
                    .string("role")
                    .map(|r| r.unwrap_or_else(|| "".to_string()))
                    .map_err(|_r| "".to_string()));

        for (role, creeps) in &grouping {
            self.creeps.set(role.unwrap(), creeps.collect());
        }
    }

    fn can_spawn(&self) -> bool {
        !self.spawns.is_empty()
    }

    fn spawn_miners_and_trucks(&mut self) {
        let spawn = &self.spawns[0];
        let mut sources = self.room.find(find::SOURCES);
        if sources.is_empty() { return; }

        sources.sort_by(|a, b| Math::dist(a, spawn).cmp(&Math::dist(b, spawn)));

        let miners = self.creeps.get("miner".to_string()).map_or_else(|| CreepStats::default(), |c| CreepStats::from_creeps(c));
        let trucks = self.creeps.get("truck".to_string()).map_or_else(|| CreepStats::default(), |c| CreepStats::from_creeps(c));
        let required_work = SpawnController::get_energy_per_tick(&sources[0]) / HARVEST_POWER as u8;
        let path_to_storage = self.path_length_to_storage(&source[0]);
    }

    fn get_energy_per_tick(source: &Source) -> u8 {
        (source.energy_capacity() as f32 / 300.0).ceil() as u8
    }

    fn path_length_to_storage(&self, source: &Source) -> u8 {
        let spawn = &self.spawns[0];
        PathFinder.search()
    }
}

struct CreepStats {
    work: u8,
    r#move: u8,
    carry: u8,
    attack: u8,
    ranged_attack: u8,
    heal: u8,
    claim: u8,
    tough: u8
}

impl CreepStats {
    pub fn default() -> CreepStats {
        CreepStats {
            work: 0,
            r#move: 0,
            carry: 0,
            attack: 0,
            ranged_attack: 0,
            heal: 0,
            claim: 0,
            tough: 0
        }
    }

    pub fn from_creeps(creeps: &Vec<Creep>) -> CreepStats {
        let mut stats = CreepStats::default();
        creeps.iter().for_each(|creep| {
            creep.body().iter().for_each(|p| {
                match p.part {
                    Part::Move => stats.r#move += 1,
                    Part::Work => stats.work += 1,
                    Part::Carry => stats.carry += 1,
                    Part::Attack => stats.attack += 1,
                    Part::RangedAttack => stats.ranged_attack += 1,
                    Part::Tough => stats.tough += 1,
                    Part::Heal => stats.heal += 1,
                    Part::Claim => stats.claim += 1,
                }
            });
        });

        stats
    }

    pub fn from_creep(creep: Creep) -> CreepStats {
        let mut stats = CreepStats::default();
        creep.body().iter().for_each(|p| {
            match p.part {
                Part::Move => stats.r#move += 1,
                Part::Work => stats.work += 1,
                Part::Carry => stats.carry += 1,
                Part::Attack => stats.attack += 1,
                Part::RangedAttack => stats.ranged_attack += 1,
                Part::Tough => stats.tough += 1,
                Part::Heal => stats.heal += 1,
                Part::Claim => stats.claim += 1,
            }
        });        

        stats
    }
}