FROM rust 

RUN rustup toolchain install nightly
RUN rustup self update
RUN cargo install cargo-screeps
RUN cargo install cargo-web

CMD tail -f /dev/null
#ENTRYPOINT ["bash", "tail", "-f" "/dev/null"]